export class AsciiConverter {
    convert(str, remainingNumberOfCalls = 10) {
        if (remainingNumberOfCalls < 1) {
            return ''
        }
        let combining = /[\u0300-\u036F]/g
        str = str.normalize('NFKD').replace(combining, '')

        if (!isAscii(str)) {
            const str2 = additionalReplace(str)
            if (str2 === str) {
                return ''
            }
            return this.convert(str2, remainingNumberOfCalls - 1)
        }
        return str
    }
}

function isAscii(str) {
    return /^[\x00-\x7F]*$/.test(str)
}

function additionalReplace(str) {
    const characterMap = {
        /* Latin epsilon */
        'Ɛ': 'E',
        'ɛ': 'e',
        /* Reversed E */
        'Ǝ': 'E',
        'ǝ': 'e',
        /* Schwa */
        'Ə': 'E',
        'ə': 'e',
        /* B with hook */
        'Ɓ': 'B',
        'ɓ': 'b',
        /* I with bar */
        'Ɨ': 'I',
        'ɨ': 'i',
        /* I with ogonek */
        'Į': 'I',
        'į': 'i',
        /* I (?) */
        'ı̨': 'i',
        /* I (dotless) */
        'I': 'I',
        'ı': 'i',
        /* L with stroke */
        'Ł': 'L',
        'ł': 'l',
        /* Eng */
        'Ŋ': 'N',
        'ŋ': 'n',
        /* O with ogonek */
        'Ǫ': 'O',
        'ǫ': 'o',
        /* Alveolar click */
        'ǃ': '!',
        /* Bilabial click */
        'ʘ': '!',
        /* Dental click */
        'ǀ': '!',
        /* Lateral click */
        'ǁ': '!',
        /* Palatal click */
        'ǂ': '!',
        /* Apostrophes */
        '’': '\'',
        'ʾ': '\'',
        '′': '\'',
        'ˊ': '\'',
        'ˈ': '\'',
        'ꞌ': '\'',
        '‘': '\'',
        'ʿ': '\'',
        '‵': '\'',
        'ˋ': '\'',
        'ʼ': '\'',
    }

    const specialCharsRegex = new RegExp(Object.keys(characterMap).join('|'), 'g')

    return str.replace(specialCharsRegex, match => {
        return characterMap[match]
    })
}
