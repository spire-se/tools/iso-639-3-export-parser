import decompress from 'decompress'
import fs from 'fs'
import {native as rimrafNative} from 'rimraf'
import {sprintf} from 'sprintf-js'
import {AsciiConverter} from './asciiConverter.js'

main()

function main() {
    console.time('execution time')
    const zipPath = process.argv[2]
    const outputPath = process.argv[3]
    if (zipPath === undefined) {
        console.error('Missing parameter : path of archive')
        process.exit(1)
    }
    if (outputPath === undefined) {
        console.error('Missing parameter : path of output')
        process.exit(1)
    }
    const tempPath = 'temp_' + getRandomKey()
    decompress(zipPath, tempPath)
        .then((files) => {
            const output = processFilesContent(files)
            const macroLanguageFileContent = output.macroLanguageContent
            const nameIndexFileContent = output.nameIndexContent

            if (nameIndexFileContent.length === 0 || macroLanguageFileContent.length === 0) {
                console.log('Failure when reading archive content')
                process.exit(1)
            }

            const macroLanguageContent = loadMacroLanguageContentFromStringContent(macroLanguageFileContent)
            const languageData = loadNameIndexLanguageContentAndSetMacro(nameIndexFileContent, macroLanguageContent)
            const dataToWrite = {languages: languageData, metadata: processMetadata()}

            if (languageData.length === 0) {
                console.log('No language found, keeping temp folder ' + tempPath)
                process.exit(1)
            }

            fs.writeFileSync(outputPath, JSON.stringify(dataToWrite))

            console.log(sprintf(
                '%i languages parsed, written to %s',
                languageData.length,
                outputPath,
            ))
            console.timeEnd('execution time')

            deleteTempFolder(tempPath)
        })
        .catch((error) => {
            console.log(error)
            process.exit(1)
        })
}

function processFilesContent(files) {
    let macroLanguageContent = undefined
    let nameIndexContent = undefined

    files.forEach(file => {
        if (file.type !== 'file' || file.path.includes('__MACOSX')) {
            return
        }
        if (file.path.includes('iso-639-3-macrolanguages_')) {
            macroLanguageContent = file.data.toString()
        } else if (file.path.includes('iso-639-3_Name_Index_')) {
            nameIndexContent = file.data.toString()
        }
    })

    return {macroLanguageContent, nameIndexContent}
}

function processMetadata() {
    return {}
}

function loadMacroLanguageContentFromStringContent(content) {
    let macroLgForIndividuals = new Map()

    content.split('\r\n').map((line, index) => {
        if (index > 0) {
            const [macroId, indId, status] = line.split('\t')
            macroLgForIndividuals.set(indId, macroId)
        }
    })

    return macroLgForIndividuals
}

function loadNameIndexLanguageContentAndSetMacro(content, macroLanguageData) {
    let macroLanguagesMap = new Map()
    let languagesMap = new Map()
    let parsedLanguages = []
    const asciiConverter = new AsciiConverter()

    macroLanguageData.forEach(val => {
        macroLanguagesMap.set(val, true)
    })

    content.split('\r\n').forEach((line, index) => {
        if (index > 0) {
            const [code, name, invName] = line.split('\t')
            const alreadyReadData = languagesMap.get(code)
            const nameAscii = asciiConverter.convert(name)

            let languageData = {code, name, nameAscii}

            if (alreadyReadData === undefined) {
                const macroLanguage = macroLanguageData.get(code)
                if (macroLanguage !== undefined) {
                    languageData.macroLanguage = macroLanguage
                }
                const isMacroLanguage = macroLanguagesMap.get(code)
                if (isMacroLanguage !== undefined) {
                    languageData.isMacro = true
                }
                if (languageData.code === 'eng') {
                    languageData.isDefault = true
                }
            } else {
                languageData.name = alreadyReadData.name + ', ' + languageData.name
                languageData.nameAscii = alreadyReadData.nameAscii + ', ' + languageData.nameAscii
            }

            languagesMap.set(code, languageData)
        }
    })

    languagesMap.forEach(val => {
        parsedLanguages.push(val)
    })

    return parsedLanguages
}

function getRandomKey() {
    return (Math.random() + 1).toString(16).substr(2, 12)
}

function deleteTempFolder(path) {
    rimrafNative(path)
        .catch((e) => {
            console.log(e)
            process.exit(1)
        })
}